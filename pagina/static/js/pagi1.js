
/*pagina 1*/
var imagenes = ['http://localhost:8000/media/apple.png','http://localhost:8000/media/Huawei.jpg','http://localhost:8000/media/iPhone6s.jpg','http://localhost:8000/media/motonevision1.jpg','http://localhost:8000/media/MotoOneVision.jpg','http://localhost:8000/media/samsunggalaxya51.jpg','http://localhost:8000/media/samsunggalaxya71.jpg'],
    cont=0;/*para controlar en que imagen vamos*/

function carousel(contenedor){
    contenedor.addEventListener('click',e => {
        let atras = contenedor.querySelector('.atras'),
            adelante = contenedor.querySelector('.adelante'),
            img = contenedor.querySelector('#imagenes'),
            tgt = e.target;
        if (tgt==atras){
            if(cont>0){
                img.src=imagenes[cont - 1];
                cont--;
            }else{
                img.src= imagenes[imagenes.length - 1];
                cont=imagenes.length - 1;
            }
        }else if(tgt==adelante){
            if(cont<imagenes.length -1 ){
                img.src=imagenes[cont + 1];
                cont++;
            }else{
                img.src= imagenes[0];
                cont=0;
            }
        }

    });
}
document.addEventListener("DOMContentLoaded",()=>{
    let contenedor = document.querySelector('.carrusel');

    carousel(contenedor);
});