/*---ocultar y mostrar imagen*/
function ocultar(){
    document.getElementById('fin').style.display="none";
}
function mostrar(){
    document.getElementById('fin').style.display="block";
}












/*Pagina 3------------------------*/
function validar(){
    var txtUsuario = document.getElementById("txtUsername");
    var username= txtUsuario.value;

    
    if(username.length <1) {
        //alert("para el nombre")
        $("#eUsername").html("¡Celda del nombre vacía, escribe tus nombres!");
        return false;
    }
    /*para el apellido*/
    var txtUsuario2 = document.getElementById("txtUsername2");
    var username2= txtUsuario2.value;
    
    if(username2.length<1){
        $("#eUsername2").html("¡Celda del apellido vacía, escribe tus apellidos!");
        return false;
    }
    /*Correo-----------*/

    var correo = document.getElementById("correo").value;
    var expReg= /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{3})+$/;

    if(!expReg.test(correo)){
        $("#correoe").html("¡Correo no cumple con el formato, ejem: xxx@gmail.com!");
        return false;
    }

    /*contraseñas--------------*/
    var pw1 = $("#txtPassword").val();
    var pw2 = $("#txtPassword2").val();
     
    if(pw1.length<5){
        $("#errorcontraseña").html("¡Escribe almenos 5 caracteres de contraseña!");
        return false;
    }
    if(pw1!=pw2){
        $("#errorcontraseña2").html("¡La contraseña no coincide con la primera!");
        return false;
    }


    return true;
}
/*funcion para circulo cargando*/
window.onload=function(){
    var contenedor = document.getElementById('contenedor_carga');

    contenedor.style.visibility='hidden';
    contenedor.style.opacity='0';
}
/*window.addEventListener('load',()=>{
    const contenedor_carga = document.querySelector('#contenedor_carga')
    contenedor_carga.style.opacity = 0
    contenedor_carga.style.visibility='hidden'
});*/



/*Pagina 4 INICIO DE SESIÓN-------------------------------------*/

function validar2(){
    var correoI = document.getElementById("correoinicio").value;
    var expReg2= /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{3})+$/;

    if(!expReg2.test(correoI)){
        $("#correoinicioe").html("¡Correo no cumple con el formato, ejem: xxx@gmail.com!");
        return false;
    }
    var pwI = $("#txtPasswordI").val();
     
    if(pwI.length<5){
        $("#errorcontraseñaI").html("¡Escribe almenos 5 caracteres de contraseña!");
        return false;
    }

    return true;
}