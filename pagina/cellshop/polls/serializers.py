from .models import Celulares, Marca, Botones
from rest_framework import serializers

class BotonesSerializers(serializers.ModelSerializer):
    class Meta:
        model = Botones
        fields = '__all__'

class MarcaSerializers(serializers.ModelSerializer):
    class Meta:
        model = Marca
        fields = '__all__'

class CelularesSerializers(serializers.ModelSerializer):
    nombre_marca = serializers.CharField(read_only=True, source="marca.nombre")
    marca = MarcaSerializers(read_only=True)
    marca_id = serializers.PrimaryKeyRelatedField(queryset=Marca.objects.all(), source="marca")
    nombre = serializers.CharField(required=True, min_length=3)
    
    def validate_nombre(self, value):
        existe = Celulares.objects.filter(nombre__iexact=value).exists()

        if existe:
            raise serializers.ValidationError("Este producto ya existe")
        return value

    class Meta:
        model = Celulares
        fields = '__all__'