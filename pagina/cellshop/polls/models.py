from django.db import models

# Create your models here.
class Botones(models.Model):
    nombre = models.CharField(max_length=50)
    imagen = models.ImageField(null=True)

    def __str__(self):
        return self.nombre

class Marca(models.Model):
    nombre = models.CharField(max_length=50)
    imagen = models.ImageField(null=True)

    def __str__(self):
        return self.nombre

class Celulares(models.Model):
    nombre = models.CharField(max_length=50)
    precio= models.IntegerField()
    marca = models.ForeignKey(Marca, on_delete=models.PROTECT)
    imagen = models.ImageField()
    def __str__(self):
        return self.nombre