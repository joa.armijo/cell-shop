from django.contrib import admin
from .models import Marca, Celulares, Botones
# Register your models here.

admin.site.register(Marca)
admin.site.register(Celulares)
admin.site.register(Botones)
