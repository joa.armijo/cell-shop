from django.shortcuts import render, redirect, get_object_or_404
from .forms import CustomUserCreationForm, CelularesForm
from .models import Celulares, Marca, Botones
from django.contrib import messages
from django.core.paginator import Paginator
from django.contrib.auth import authenticate, login
from rest_framework import viewsets
from .serializers import CelularesSerializers, MarcaSerializers, BotonesSerializers
# Create your views here.

class BotonesViewsets(viewsets.ModelViewSet):
    queryset = Botones.objects.all()
    serializer_class = BotonesSerializers

class MarcaViewsets(viewsets.ModelViewSet):
    queryset = Marca.objects.all()
    serializer_class = MarcaSerializers

class CelularesViewsets(viewsets.ModelViewSet):
    queryset = Celulares.objects.all()
    serializer_class = CelularesSerializers

    def get_queryset(self):
        celulares = Celulares.objects.all()

        nombre = self.request.GET.get('nombre')

        if nombre: 
            celulares = celulares.filter(nombre__contains=nombre)
        
        return celulares    

def error_facebook(request):
    return render(request, 'registration/error_facebook.html')

def index(request):
    return render(request,'polls/home.html')

def pag2(request):
    return render(request,'polls/pagina2.html')

def registro(request):
    data = {
        'form': CustomUserCreationForm()
    }

    if request.method == 'POST':
        formulario = CustomUserCreationForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            user = authenticate(username=formulario.cleaned_data["username"], password=formulario.cleaned_data["password1"])
            login(request, user)
            messages.success(request, "Te has registrado correctamente")
            return redirect(to='http://localhost:8000/polls/')
        data["form"] = formulario

    return render(request,'registration/pagina3.html', data)

def logins(request):
    return render(request,'registration/login.html')

def agregar_producto(request):
    data = {
        'form': CelularesForm()
    }

    if request.method == 'POST':
        formularioc = CelularesForm(data=request.POST, files=request.FILES)
        if formularioc.is_valid():
            formularioc.save()
        else:
            data["form"] = formularioc

    return render(request, 'polls/agregar.html', data)

def listar_productos(request):
    celulares = Celulares.objects.all()

    data = {
        'celulares': celulares
    }
    return render(request, 'polls/listar.html', data)

def modificar_producto(request, id):

    celular =get_object_or_404(Celulares, id=id)

    data = {
        'form': CelularesForm(instance=celular)
    }

    if request.method == 'POST':
        formulariom = CelularesForm(data=request.POST, instance=celular, files=request.FILES)
        if formulariom.is_valid():
            formulariom.save()
            return redirect(to='http://localhost:8000/polls/listar')
        data["form"] = formulariom


    return render(request, 'polls/modificar.html', data)

def eliminar_producto(request, id):
    celulare = get_object_or_404(Celulares, id=id)
    celulare.delete()
    return redirect(to='http://localhost:8000/polls/listar' )    
