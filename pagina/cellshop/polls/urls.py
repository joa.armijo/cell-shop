from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('celulares', views.CelularesViewsets)
router.register('marca', views.MarcaViewsets)
router.register('botones', views.BotonesViewsets)

#localhost:8000/polls/api/celulares/
urlpatterns = [
    path('', views.index, name='index'),
    path('1', views.pag2, name='pag2'),
    path('registro', views.registro, name='registro'),
   # path('accounts/login/', views.login, name='login'),
   path('agregar',views.agregar_producto,name='agregar'),
   path('listar', views.listar_productos,name='listar'),
   path('modificar/<id>/', views.modificar_producto,name='modificar'),
   path('eliminar/<id>/', views.eliminar_producto,name='eliminar'),
   
   path('reset_password/', auth_views.PasswordResetView.as_view(template_name="registration/password_reset.html"), name="reset_password"),
   path('reset_password_sent/', auth_views.PasswordResetDoneView.as_view(template_name="registration/password_reset_sent.html"), name="password_reset_done"),
   path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name="registration/password_reset_form.html"), name="password_reset_confirm"),
   path('reset_password_complete/', auth_views.PasswordResetCompleteView.as_view(template_name="registration/password_reset_done.html"), name="password_reset_complete"),

   path('api/', include(router.urls)),
   
   path('error-facebook/', views.error_facebook, name='error_facebook'),
]