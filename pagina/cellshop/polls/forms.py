from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Celulares

class CustomUserCreationForm(UserCreationForm):
    
    class Meta:
        model = User
        fields = ["username","first_name","last_name","email","password1","password2"]


class CelularesForm(forms.ModelForm):
    
    class Meta:
        model = Celulares
        fields = '__all__'