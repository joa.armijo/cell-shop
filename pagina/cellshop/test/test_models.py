from django.test import TestCase

from polls.models import Botones, Marca

class BotonesModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Botones.objects.create(nombre='close', imagen='close.png')

    def test_nombre_label(self):
        botones=Botones.objects.get(id=1)
        field_label = botones._meta.get_field('nombre').verbose_name
        self.assertEquals(field_label,'nombre')

    def test_imagen(self):
        botones=Botones.objects.get(id=1)
        field_label = botones._meta.get_field('imagen').verbose_name
        self.assertEquals(field_label,'imagen')

class MarcaModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Marca.objects.create(nombre='Samsung', imagen='samsung.jpg')

    def test_nombre_label(self):
        marca=Marca.objects.get(id=1)
        field_label = marca._meta.get_field('nombre').verbose_name
        self.assertEquals(field_label,'nombre')

    def test_imagen(self):
        marca=Marca.objects.get(id=1)
        field_label = marca._meta.get_field('imagen').verbose_name
        self.assertEquals(field_label,'imagen')

