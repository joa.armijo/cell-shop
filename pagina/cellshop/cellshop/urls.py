from django.contrib import admin
from django.urls import include,path
from django.conf import settings
from django.conf.urls.static import static
#from vistas import index,pag2,pag3,pag4

urlpatterns = [
    path('polls/', include('polls.urls')),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('social-auth/', include('social_django.urls', namespace="social")),
    path('', include('pwa.urls')),
   # path('index/', index),
    #path('pag2/',pag2),
    #path('pag3/',pag3),
    #path('pag4/',pag4),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)